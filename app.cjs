
fetch('https://restcountries.com/v3.1/all')
    .then(function (response) {
        return response.json()
    })
    .then(function (data) {
        document.getElementsByClassName("lds-ripple")[0].style.display = "none";
        addToCountriesContainer(data)
    })
    .catch(function (err) {
        document.getElementsByClassName("lds-ripple")[0].style.display = "none";
        errorMessage(err)
    })


const filterRegion = document.getElementById('filter')

filterRegion.addEventListener('change', function () {
    filterCountries(filterRegion.value)
})



function addToCountriesContainer(countries) {
    let countriesContainer = document.getElementById('countries-container')

    countries.forEach((country, index) => {

        let countryDiv = document.createElement('div')
        countryDiv.className = 'country'

        let flagURL = country.flags.png

        let countryName = country.name.common

        let population = country.population

        let region = country.region

        let capital = country.capital

        let nativeName = ""
        if (country.name.nativeName !== undefined && typeof country.name.nativeName === 'object') {
            // console.log(Object.entries(country.name.nativeName))
            let temp = Object.entries(country.name.nativeName)
            if (Array.isArray(temp) && temp) {
                nativeName = temp[0][1].common
            }
        }
        else {
            nativeName = countryName
        }

        let subRegion = country.subregion

        let tld = country.tld

        let currencies
        if (country.currencies !== undefined) {
            let temp = Object.entries(country.currencies)
            currencies = temp.map((currency, index) => {
                // console.log(currency)
                return currency[1].name
            })
        }
        else {
            currencies = ['-']
        }
        currencies = currencies.join(', ')

        let languages
        if (country.languages !== undefined) {
            languages = Object.values(country.languages)
        }
        else {

            languages = ['-']
        }
        languages = languages.join(', ')


        let imgDiv = document.createElement('img')
        imgDiv.className = 'flag-image'
        imgDiv.setAttribute('src', flagURL)
        imgDiv.setAttribute('alt', countryName)
        imgDiv.addEventListener('click', popup)



        let countryNameDiv = document.createElement('div')
        countryNameDiv.className = 'country-name'
        countryNameDiv.appendChild(document.createTextNode(countryName))


        let populationDiv = document.createElement('div')
        populationDiv.className = 'info-text'
        populationDiv.appendChild(document.createTextNode(`Population: ${population}`))

        let regionDiv = document.createElement('div')
        regionDiv.className = 'info-text'
        regionDiv.appendChild(document.createTextNode(`Region: ${region}`))

        let capitalDiv = document.createElement('div')

        capitalDiv.className = 'info-text'
        capitalDiv.appendChild(document.createTextNode(`Capital: ${capital}`))




        countryDiv.appendChild(imgDiv)
        countryDiv.appendChild(countryNameDiv)
        countryDiv.appendChild(populationDiv)
        countryDiv.appendChild(regionDiv)
        countryDiv.appendChild(capitalDiv)


        countriesContainer.appendChild(countryDiv)



        // POPUP
        let popupSection = document.createElement('section')
        popupSection.id = 'popup-section'

        let popupHeaderDiv = document.createElement('div')
        popupHeaderDiv.className = 'popup-header'

        let popupButton = document.createElement('button')
        popupButton.id = 'close-button'
        popupButton.appendChild(document.createTextNode("X"))

        popupHeaderDiv.appendChild(popupButton)
        popupSection.appendChild(popupHeaderDiv)

        let popupInfoMain = document.createElement('div')
        popupInfoMain.id = 'popup-info-main'


        let popupFlag = document.createElement('img')
        popupFlag.className = 'popup-flag-image'
        popupFlag.src = flagURL
        popupFlag.alt = countryName
        popupInfoMain.appendChild(popupFlag)

        let popupInfoDiv = document.createElement('div')
        popupInfoDiv.className = 'popup-info'

        let popupCountryName = document.createElement('span')

        popupCountryName.className = 'country-name'
        popupCountryName.appendChild(document.createTextNode(countryName))
        popupInfoDiv.appendChild(popupCountryName)

        let popupInfoList = document.createElement('ul')

        let popupInfoNativeName = document.createElement('li')
        popupInfoNativeName.appendChild(document.createTextNode(`Native Name: ${nativeName}`))
        popupInfoList.appendChild(popupInfoNativeName)


        let popupInfoTld = document.createElement('li')
        popupInfoTld.appendChild(document.createTextNode(`Top Level Domain: ${tld}`))
        popupInfoList.appendChild(popupInfoTld)

        let popupInfoPopulation = document.createElement('li')
        popupInfoPopulation.appendChild(document.createTextNode(`Population: ${population}`))
        popupInfoList.appendChild(popupInfoPopulation)

        let popupInfoCurrencies = document.createElement('li')
        popupInfoCurrencies.appendChild(document.createTextNode(`Currencies: ${currencies}`))
        popupInfoList.appendChild(popupInfoCurrencies)

        let popupInfoRegion = document.createElement('li')
        popupInfoRegion.appendChild(document.createTextNode(`Region: ${region}`))
        popupInfoList.appendChild(popupInfoRegion)

        let popupInfoLanguage = document.createElement('li')
        popupInfoLanguage.appendChild(document.createTextNode(`Languages: ${languages}`))
        popupInfoList.appendChild(popupInfoLanguage)

        let popupInfoSubRegion = document.createElement('li')
        popupInfoSubRegion.appendChild(document.createTextNode(`Sub Region: ${subRegion}`))
        popupInfoList.appendChild(popupInfoSubRegion)

        let popupInfoCapital = document.createElement('li')
        popupInfoCapital.appendChild(document.createTextNode(`Capital: ${capital}`))
        popupInfoList.appendChild(popupInfoCapital)

        popupInfoDiv.appendChild(popupInfoList)
        popupInfoMain.appendChild(popupInfoDiv)
        popupSection.appendChild(popupInfoMain)

        let overlay = document.createElement("div")
        overlay.id = "overlay"
        countryDiv.appendChild(overlay)

        countryDiv.appendChild(popupSection)

    })
}

function errorMessage(err) {
    let errorDiv = document.createElement('div')
    let countriesContainer = document.getElementById('countries-container')
    let inputFilter = document.getElementsByClassName('input-filter')
    inputFilter[0].style.display = 'none'
    errorDiv.className = 'error'
    errorDiv.appendChild(document.createTextNode(`Sorry! Your request can't be completed. Error while fetching data.`))

    countriesContainer.appendChild(errorDiv)
}

function filterCountries(selectedRegion) {
    let countries = document.getElementsByClassName('country')
    if (selectedRegion === 'All') {
        location.reload()
    }
    for (let country of countries) {
        let currRegion = country.childNodes[3].textContent.slice(8)

        if (currRegion == selectedRegion) {

            country.style.display = 'block'
        }
        else {
            country.style.display = 'none'
        }
    }
}


function popup(event) {

    // console.log(event.target.parentNode.childNodes[5])
    let overlay = event.target.parentNode.childNodes[5]
    let popupSection = overlay.nextElementSibling
    let closeBtn = popupSection.firstChild.firstChild

    overlay.classList.add("overlay")
    popupSection.classList.add("show")

    document.addEventListener("keydown", function (e) {
        if (e.key == "Escape") {
            closePopup(popupSection, overlay)
        }
    })
    overlay.addEventListener("click", function (e) {
        closePopup(popupSection, overlay)
    })
    closeBtn.addEventListener('click', () => {
        closePopup(popupSection, overlay)
    })
}

function closePopup(popup, overlay) {
    popup.classList.remove("show")
    overlay.classList.remove("overlay")
}



